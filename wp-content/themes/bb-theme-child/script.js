var $ = jQuery;

function fixedHeaderTopSpacing(){
    if( $('.top-bar-1').length!=0 && ($('header.fl-page-header-primary').css('position') == "absolute" || $('header.fl-page-header-primary').css('position') == "fixed") ){
        $('header.fl-page-header-primary').css('top', $('.top-bar-1').outerHeight(true));
        if($('.banner-row').length>=1 || $('.product-banner-img').length>=1){
            $('.fl-page').addClass('add-mobile-pad-top');
        }
        else{
            $('.fl-page').css('padding-top', ($('.top-bar-1').outerHeight(true) + $('header.fl-page-header-primary').outerHeight(true)));
        }
    }
}
$(document).ready(function() {
    fixedHeaderTopSpacing();
    $(window).resize(function(){
        fixedHeaderTopSpacing();
    });
});